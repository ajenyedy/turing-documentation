# Submitting a SLURM Script

??? note "TLDR"
    Submit your job script using `sbatch my_submission_script.sh`, check its status with `squeue --me`, monitor outputs with `watch tail my_output_file.out`, and cancel jobs with `scancel` if needed.

Once you have completed your SLURM submission script, save it as a `.sh` file. For example, if you have a submission script named `run.sh`, you can submit it by running:

```bash
sbatch run.sh
```

in the Turing terminal window. Your job will be placed in the scheduler queue and will run when resources become available.



---

## 🖥️ Monitoring Your Job

### **Checking Job Status**

To check the status of your running and queued jobs, use the command:

```bash
squeue --me
```

This command will show the status of only your own jobs.

you can use the `watch` command to look at the output in real time

```bash
watch squeue --me
```

!!! tip Cancelling a running command
    you can cancel any running command using ctrl-c inside the terminal. This kills the activly running process.

### **Output and Error Files**

When you submit a job, SLURM redirects your job's standard output and standard error to files. By default, these files are combined into `slurm-<job_id>.out`.

**Customizing Output Files:**

You can specify custom filenames for these outputs in your submission script using the following directives:

```bash
#SBATCH --output my_job_output-%j.out    # Standard Output file
#SBATCH --error my_job_errors-%j.err    # Standard Error file
```

### **Monitoring Job Output in Real-Time**

the `tail` command shows you the last few lines of a file. Combining this with `watch` lets you watch the output of your job in real time.

```bash
watch tail my_job_output.out
```

---

## ❌ Cancelling a Job

If you need to cancel a job—for example, if it's using too many resources or behaving unexpectedly—you can use the `scancel` command:

```bash
scancel <job_id>
```

Replace `<job_id>` with the actual job ID assigned by SLURM when you submitted the job. You can find the job id using `squeue --me`

**Example:**

```bash
scancel 123456
```

This will cancel the job with ID `123456`.



    