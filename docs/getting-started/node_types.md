# Login Nodes vs. Compute Nodes

??? note "TLDR"
    use the login node to set up a job, but do heavy computation on compute nodes

When you login to Turing, you're connected to a **login node**. Think of the login node as the front desk of a large computational facility:


## 🖥️ **Login Node**

- **Purpose**: Meant for lightweight tasks such as:
  - Editing files
  - Compiling code
  - Submitting jobs to the scheduler
- **🚫 Important**: **Do not run heavy computations here.** It is innefecient and can make it harder for others to use turing.


## 🏭 **Compute Nodes**

- **Purpose**: The powerhouse where your computational jobs are executed.
- **Access**: Not directly accessible via SSH, instead, jobs are sent here through the job scheduler, SLURM.
