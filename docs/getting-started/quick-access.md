---
hide:
  - navigation
  - toc
---
### Quick Access Steps:
1. **Request Access**: Complete the [Turing Account Request Form](https://arc.wpi.edu/computing/accounts/turing-accounts/).  
2. **[Connect to Turing](../getting-started/index.md)**: Log in via SSH:  
   ```bash
   ssh gompei@turing.wpi.edu
   ```
3. **[Write a Job Script](../getting-started/writing.md)**: Example SLURM script:  
   ```bash
   #!/bin/bash
   #SBATCH --job-name=my_job
   #SBATCH --time=01:00:00
   #SBATCH --mem=4G
   module load python
   python my_script.py
   ```
4. **[Submit the Job](../getting-started/submitting.md)**:  
   ```bash
   sbatch my_job_script.sh
   ```  
