# Linux on Turing

??? note "TLDR"
    Turing runs on Linux, specifically Ubuntu 20.04.6 LTS. Essential commands include `ls` to list files, `cd` to change directories, `mkdir` to create directories, `ssh` and `scp` for remote access and file transfer, and `nano` for text editing.

Turing runs on Linux, specifically **Ubuntu 20.04.6 LTS**. To use Turing effectively, you'll need to know some basics of navigating a text-based Linux environment.

---

## Terminology
- **🖥️ Terminal/Bash**: A command-line interface where users type instructions to interact with the operating system, automate tasks, and run scripts. The default shell on Turing is **bash**.
- **📂 Directory**: A folder in the file system used to organize and store files. Think of directories as containers for other files and subdirectories. For example, `/home/gompei` is a directory that might contain your project files.
- **💻 Command**: Instructions you type in the bash terminal to perform tasks, such as listing files or creating directories. Type the command in your terminal and press **Enter/Return** to execute it.

---

## Essential Commands
Here are some fundamental Linux commands to get started:

1. **📄 `ls`**: Lists files and directories in the current directory.
   ```bash
   ls
   ```
   Use `ls -l` for a detailed list or `ls -a` to include hidden files.

2. **📂 `cd`**: Changes the current directory.
   ```bash
   cd /path/to/directory
   ```
   Use `cd ~` to return to your home directory or `cd ..` to move up one level.

3. **📁 `mkdir`**: Creates a new directory.
   ```bash
   mkdir new_directory_name
   ```
   Example: `mkdir my_project`.

4. **🔐 `ssh`**: Connects to a remote system via Secure Shell.
   ```bash
   ssh gompei@turing.wpi.edu
   ```

5. **📤 `scp`**: Securely copies files between your local system and a remote server and vice cersa.
   ```bash
   scp file.txt gompei@turing.wpi.edu:~/remote/path
   ```
   Use it to transfer files to or from Turing.

6. **✏️ `nano`**: A simple text editor for modifying files directly in the terminal.
   ```bash
   nano filename.txt
   ```
   Note that if filename.txt doesnt already exist this will create it. Use `Ctrl+O` to save and `Ctrl+X` to exit.

---

!!! tip " Terminal Usage Pro Tips"
    - **🚀 Tab Completion**: Press `Tab` to autocomplete commands and file/directory names.
    - **🔄 Up Arrow**: Scroll through your command history.
    - **📖 `man <command>`**: View the manual for any command (e.g., `man ls`).