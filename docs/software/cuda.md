# Using GPUs and CUDA drivers

If your job involves training a neural network on a large dataset, it is highly
recommended that you request CUDA drivers to accelerate the training process. To
access CUDA drivers, first load them in your SLURM script using:

```bash
module load cuda
```

Or, you may wish to use a specific CUDA version or add specific CUDA libraries,
which you can load by using the following lines instead:

```bash
module load cuda11.6/blas/ # (1)!
module load cuda11.6/fft/ # (2)!
module load cuda11.6/toolkit/ # (3)!
```

1. `cuda11.6/blas/` loads the CUDA Basic Linear Algebra Subroutines library for
   matrix and vector operations
2. `cuda11.6/fft/` loads the CUDA Fast Fourier Transform library for signal and
   image processing
3. `cuda11.6/toolkit/` loads the entire CUDA toolkit necessary for using NVIDIA
   GPUs

To complete the neural network’s training process, you may require minimum
specific hardware to ensure the job is handled properly. To do so, add the
following line to your SLURM script

```bash
#SBATCH --gres=gpu:1
#SBATCH -C A100|V100
```

with the other `#SBATCH` lines to specify that this job must be run on an NVIDIA
A100 or V100 GPU.
