# Running Graphical Applications

!!! tip
    Turing is a terminal-first environment designed primarily for batch processing.
    In general, if an interactive graphical session is needed, we recommend using
    our [Windows terminal servers](https://arc.wpi.edu/computing/windows-servers/).
    However, there are some instances where running a graphical Turing session may
    be of some use.

## Using `apptainer-desktop`

!!! warning
    This feature is experimental and still undergoing development and testing.
    If you encounter any issues, please [email ARC](mailto:gr-arcunix@wpi.edu)
    and let us know so that we can resolve them as quickly as possible.

### Starting a graphical session

To open a graphical session on Turing, run the following commands on a login
node:

```bash
module load apptainer-desktop
desktop
```

This will generate a URL. Navigating to that URL will prompt you to log in, and
then give you access to a bare-bones desktop environment.

!!! note
    Occasionally the login process will fail. If you see an `Internal Server
    Error` message, re-navigate to the link you copied earlier.
    

### Running applications

In the lower-left-hand corner of the desktop, there is a button to launch a
Windows-like "Start Menu". A number of utility applications are installed by
default. Any software available as a Turing
[module](../software/modules.md) can be loaded in the same manner as
usual in the `QTerminal` application, which is located in the start menu under
`System Tools`.
