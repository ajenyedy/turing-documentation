# Turing Documentation

https://docs.turing.wpi.edu

## Local Development

```bash
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
mkdocs serve
```

This documentation is built using mkdocs-materal, which is documented
[here](https://squidfunk.github.io/mkdocs-material/).
